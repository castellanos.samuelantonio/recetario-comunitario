create database RecetarioComunitario;


create table receta(
id int identity(1,1) not null primary key, 
tiempoDePreparacion int not null,
fotografia varchar(MAX) not null,
ingredientes varchar(MAX) not null,
calificacion float not null,
nacionalidad varchar(50) not null,
);

create table roles(
id int identity(1,1) not null primary key, 
nombreDeRol varchar (20) not null,
);

create table usuario(
id int identity(1,1) not null primary key, 
nombreDeUsuario varchar (16) not null,
contraseņa varchar (MAX) not null,
redesSociales varchar (MAX) not null,
avatar varchar (MAX) not null,
nacionalidad varchar (50) not null,
biografia varchar (500) not null,
cuentaCreadaEn datetime default getdate() not null,
rolDeUsuario int foreign key references roles(id)
)

create table recetasCreadas(
id int identity(1,1) not null primary key, 
autor int foreign key references usuario(id),
receta int foreign key references receta(id),
creadaEn datetime default getdate() not null
)

create table tickets (
id int identity(1,1) not null primary key, 
asunto varchar(100) not null,
contenido varchar(500) not null,
usuario int foreign key references usuario(id),
receta int foreign key references receta(id),
)


create table grupos(
id int identity(1,1) not null primary key, 
nombre varchar (50) not null,
descripcion varchar (500) not null,
)

create table administradores (
id int identity(1,1) not null primary key, 
usuario int foreign key references usuario(id),
grupo int foreign key references grupos(id),
)

create table gruposUsuarios (
id int identity(1,1) not null primary key, 
usuario int foreign key references usuario(id),
grupo int foreign key references grupos(id),
)

create table grupoPublicaciones (
id int identity(1,1) not null primary key, 
usuario int foreign key references usuario(id),
receta int foreign key references receta(id),
grupo int foreign key references grupos(id),
)